﻿class SpellChecker
{
    static void Main(string[] args)
    {
        // Чтение словаря
        List<string> dictionary = new List<string>();
        string line;
        while ((line = Console.ReadLine()) != "===")
        {
            dictionary.AddRange(line.Split(' ', StringSplitOptions.RemoveEmptyEntries));
        }

        // Чтение строк для проверки
        List<string> textLines = new List<string>();
        while ((line = Console.ReadLine()) != "===")
        {
            textLines.Add(line);
        }

        // Преобразование словаря в нижний регистр для проверки
        HashSet<string> dictSet = new HashSet<string>(dictionary.Select(word => word.ToLower()));

        // Обработка каждой строки текста
        foreach (string textLine in textLines)
        {
            string[] words = textLine.Split(' ', StringSplitOptions.RemoveEmptyEntries);
            List<string> outputWords = new List<string>();

            foreach (string word in words)
            {
                bool foundInDictionary = false;
                string originalWord = word;
                string lowerWord = word.ToLower();

                if (dictSet.Contains(lowerWord))
                {
                    outputWords.Add(word); // Слово в словаре, добавляем как есть
                }
                else
                {
                    List<string> oneEditCandidates = new List<string>();
                    List<string> twoEditCandidates = new List<string>();

                    foreach (string dictWord in dictionary)
                    {
                        string lowerDictWord = dictWord.ToLower();
                        if (IsOneEditAway(lowerWord, lowerDictWord))
                        {
                            oneEditCandidates.Add(dictWord);
                        }
                        else if (IsTwoEditsAway(lowerWord, lowerDictWord))
                        {
                            twoEditCandidates.Add(dictWord);
                        }
                    }

                    if (oneEditCandidates.Count > 0)
                    {
                        outputWords.Add(FormatCandidates(oneEditCandidates));
                    }
                    else if (twoEditCandidates.Count > 0)
                    {
                        outputWords.Add(FormatCandidates(twoEditCandidates));
                    }
                    else
                    {
                        outputWords.Add($"{{{originalWord}?}}");
                    }
                }
            }

            Console.WriteLine(string.Join(' ', outputWords));
        }
    }

    static string FormatCandidates(List<string> candidates)
    {
        if (candidates.Count == 1)
        {
            return candidates[0];
        }
        return $"{{{string.Join(' ', candidates)}}}";
    }

    static bool IsOneEditAway(string s1, string s2)
    {
        if (Math.Abs(s1.Length - s2.Length) > 1) return false;

        int edits = 0;
        int i = 0, j = 0;
        while (i < s1.Length && j < s2.Length)
        {
            if (s1[i] != s2[j])
            {
                if (++edits > 1) return false;
                if (s1.Length > s2.Length) i++;
                else if (s1.Length < s2.Length) j++;
                else { i++; j++; }
            }
            else
            {
                i++;
                j++;
            }
        }
        return edits <= 1;
    }

    static bool IsTwoEditsAway(string s1, string s2)
    {
        if (Math.Abs(s1.Length - s2.Length) > 2) return false;

        int edits = 0;
        int i = 0, j = 0;
        while (i < s1.Length && j < s2.Length)
        {
            if (s1[i] != s2[j])
            {
                if (++edits > 2) return false;
                if (s1.Length > s2.Length) i++;
                else if (s1.Length < s2.Length) j++;
                else { i++; j++; }
            }
            else
            {
                i++;
                j++;
            }
        }
        return edits <= 2;
    }
}
